package com.example.mylibrary.Data;

import android.content.Context;
import android.content.Intent;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.example.mylibrary.BookActivity;
import com.example.mylibrary.R;
import com.example.mylibrary.Utils2;

import java.util.ArrayList;


public class BookRecViewAdapter extends RecyclerView.Adapter<BookRecViewAdapter.ViewHolder> {

    private ArrayList<Book> books = new ArrayList<>();
    private Context mContext;
    private String parentActivity;

    public BookRecViewAdapter(Context mContext) {
        this.mContext = mContext;
        this.parentActivity = "";
    }

    public BookRecViewAdapter(Context mContext, String parentActivity) {
        this.mContext = mContext;
        this.parentActivity = parentActivity;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_book, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public int getImage(String imageName) {
        int drawableResourceId = mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
        return drawableResourceId;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtName.setText(books.get(position).getName());
        Log.d("TAG1", books.get(position).getImageUrl());

        String img_name = books.get(position).getImageUrl();
        int id_url = getImage(img_name);

        Glide.with(mContext).asBitmap().load(id_url).into(holder.imgBook);

        holder.txtAuthor.setText(books.get(position).getAuthor());
        holder.txtDescription.setText(books.get(position).getShortDesc());

        if(books.get(position).isExpanded()){
            TransitionManager.beginDelayedTransition(holder.parent);
            holder.expandedRelLayout.setVisibility(View.VISIBLE);
            holder.upArrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24);

            if(parentActivity == ""){
                holder.txtDelete.setVisibility(View.GONE);
            }else if (parentActivity == "alreadyRead"){
                holder.txtDelete.setVisibility(View.VISIBLE);
                holder.txtDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(Utils2.getInstance().delFromAlreadyRead(books.get(position))){
                            Toast.makeText(mContext, "Removed", Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(mContext, "Something wrong happened", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

        }else{
            TransitionManager.beginDelayedTransition(holder.parent);
            holder.expandedRelLayout.setVisibility(View.GONE);
            holder.upArrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
        }
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        private CardView parent;
        private ImageView imgBook;
        private TextView txtName;

        private ImageView downArrow, upArrow, downArrow2;
        private RelativeLayout expandedRelLayout;
        private TextView txtAuthor, txtDescription, txtDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            parent = itemView.findViewById(R.id.cardParent);
            imgBook = itemView.findViewById(R.id.imgBook);
            txtName = itemView.findViewById(R.id.txtBookName);

            expandedRelLayout = itemView.findViewById(R.id.expandedRelLayout);
            txtAuthor = itemView.findViewById(R.id.txtAuthor);
            txtDescription = itemView.findViewById(R.id.txtShortDesc);
            //downArrow = itemView.findViewById(R.id.dwnBtn);
            upArrow = itemView.findViewById(R.id.upBtn);
            downArrow2 = itemView.findViewById(R.id.upBtn2);

            txtDelete = itemView.findViewById(R.id.tvDelete);

            upArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Book book = books.get(getAdapterPosition());
                    book.setExpanded(!book.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });

//            downArrow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Book book = books.get(getAdapterPosition());
//                    book.setExpanded(!book.isExpanded());
//                    notifyItemChanged(getAdapterPosition());
//                }
//            });

            parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, BookActivity.class);
                    Book book = books.get(getAdapterPosition());
                    intent.putExtra("bookId", book.getId());
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
