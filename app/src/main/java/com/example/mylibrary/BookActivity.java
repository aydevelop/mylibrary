package com.example.mylibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.mylibrary.Data.Book;

import java.util.ArrayList;

public class BookActivity extends AppCompatActivity {

    private TextView tv1, tv2, tv3, tv4, tv5;
    private Button btnWantToRead, btnAddToAlreadyRead, btnAddToCurrentlyReading, btnAddToFavorite;
    private ImageView bookImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + getString(R.string.app_name) + "</font>"));
        initViews();

        Intent intent = getIntent();
        if (intent != null) {
            int bookId = intent.getIntExtra("bookId", -1);
            if (bookId != -1) {
                Book bk = Utils2.getInstance().getBookById(bookId);
                if(bk != null){
                    setData(bk);
                    handleAlreadyRead(bk);
                    handleWantToReadBooks(bk);
                    handleCurrentlyReadingBooks(bk);
                    handleFavoriteBooks(bk);
                }
            }
        }
    }

    private void handleFavoriteBooks(Book book)
    {

    }

    private void handleCurrentlyReadingBooks(Book book)
    {

    }

    private void handleWantToReadBooks(final Book book)
    {
        ArrayList<Book> wantToReadBooks = Utils2.getInstance().getWantToReadBooks();
        boolean existInAleadyReadBooks = false;

        for (Book b: wantToReadBooks){
            if(b.getId() == book.getId()){
                existInAleadyReadBooks = true;
            }
        }

        if(existInAleadyReadBooks){
            btnWantToRead.setEnabled(false);
        } else {
            btnWantToRead.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(Utils2.getInstance().addToWantRead(book)){
                        btnWantToRead.setEnabled(false);
                        Toast.makeText(BookActivity.this, "Book Added", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(BookActivity.this, WantToReadActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(BookActivity.this, "Something wrong happened", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void handleAlreadyRead(final Book book)
    {
        ArrayList<Book> alreadyReadBooks = Utils2.getInstance().getAlreadyReadBooks();
        boolean existInAleadyReadBooks = false;

        for (Book b: alreadyReadBooks){
            if(b.getId() == book.getId()){
                existInAleadyReadBooks = true;
            }
        }

        if(existInAleadyReadBooks){
            btnAddToAlreadyRead.setEnabled(false);
        } else {
            btnAddToAlreadyRead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Utils2.getInstance().addToAlreadyRead(book)){
                        btnAddToAlreadyRead.setEnabled(false);
                        Toast.makeText(BookActivity.this, "Book Added", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(BookActivity.this, AlreadyReadBookActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(BookActivity.this, "Something wrong happened", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    public int getImage(String imageName) {
        int drawableResourceId = this.getResources().getIdentifier(imageName, "drawable", this.getPackageName());
        return drawableResourceId;
    }

    private void setData(Book book)
    {
        tv1.setText(tv1.getText() + book.getName());
        tv2.setText(tv2.getText() + book.getAuthor());
        tv3.setText(tv3.getText() + String.valueOf(book.getPages()));
        tv4.setText(tv4.getText() + book.getShortDesc());

        String img_name = book.getImageUrl();
        int id_url = getImage(img_name);

        try {
            Glide.with(this).asBitmap().load(id_url).into(bookImage);
        }
        catch(Exception e) {
            Log.d("TAG1", e.getMessage());
        }

    }

    private void initViews()
    {
        tv1 = findViewById(R.id.textBook1);
        tv2 = findViewById(R.id.textBook2);
        tv3 = findViewById(R.id.textBook3);
        tv4 = findViewById(R.id.textBook4);

        btnWantToRead = findViewById(R.id.btnWantToRead);
        btnAddToAlreadyRead = findViewById(R.id.btnAlreadyRead);
        btnAddToCurrentlyReading = findViewById(R.id.btnAddToCurrentlyReading);
        btnAddToFavorite = findViewById(R.id.btnAddToFavorite);

        bookImage = findViewById(R.id.image2View);
    }
}