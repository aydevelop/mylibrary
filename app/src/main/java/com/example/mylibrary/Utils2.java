package com.example.mylibrary;

import com.example.mylibrary.Data.Book;

import java.util.ArrayList;

public class Utils2 {
    private static Utils2 instance;
    private static ArrayList<Book> allBooks;
    private static ArrayList<Book> alreadyReadBooks;
    private static ArrayList<Book> wantToReadBooks;
    private static ArrayList<Book> currentlyReadingBooks;
    private static ArrayList<Book> favoriteBooks;

    public Utils2()
    {
        if (allBooks == null) {
            allBooks = new ArrayList<>();
            initData();
        }

        if (alreadyReadBooks == null) {
            alreadyReadBooks = new ArrayList<>();
        }

        if (wantToReadBooks == null) {
            wantToReadBooks = new ArrayList<>();
        }

        if (currentlyReadingBooks == null) {
            currentlyReadingBooks = new ArrayList<>();
        }

        if (favoriteBooks == null) {
            favoriteBooks = new ArrayList<>();
        }
    }

    private void initData() {
        allBooks.add(new Book(1,"Ready Player Two: A Novel","Ernest Cline",423,"b1","The highly anticipated sequel to the beloved worldwide bestseller Ready Player One, the near-future adventure that inspired the blockbuster Steven Spielberg film.","longDesc1"));
        allBooks.add(new Book(2,"Earth: By The Numbers","Steve Jenkins",543,"b2","Caldecott Honor winner Steve Jenkins introduces By the Numbers infographic readers chock full of incredible infographs and stunning, full-color cut-paper illustrations. Earth will focus on the fascinating ins-and-outs of earth science.","longDesc2"));
        allBooks.add(new Book(3,"Dog Man: Grime and Punishment: From the Creator of Captain Underpants","Dav Pilkey",410,"b3","The mayor has had enough of Dog Man's shenanigans in the ninth book from worldwide bestselling author and artist Dav Pilkey.","longDesc3"));
        allBooks.add(new Book(4,"Wild Game: My Mother, Her Secret, and Me","Kathryn Harrison",341,"b4","A daughter’s tale of living in the thrall of her magnetic, complicated mother, and the chilling consequences of her complicity.","longDesc4"));
    }

    public static Utils2 getInstance() {
        if (instance == null) {
            instance = new Utils2();
        }

        return instance;
    }

    public static ArrayList<Book> getAllBooks() {
        return allBooks;
    }

    public static void setAllBooks(ArrayList<Book> allBooks) {
        Utils2.allBooks = allBooks;
    }

    public ArrayList<Book> getAlreadyReadBooks() {
        return alreadyReadBooks;
    }

    public static void setAlreadyReadBooks(ArrayList<Book> alreadyReadBooks) {
        Utils2.alreadyReadBooks = alreadyReadBooks;
    }

    public  ArrayList<Book> getWantToReadBooks() {
        return wantToReadBooks;
    }

    public static void setWantToReadBooks(ArrayList<Book> wantToReadBooks) {
        Utils2.wantToReadBooks = wantToReadBooks;
    }

    public  ArrayList<Book> getCurrentlyReadingBooks() {
        return currentlyReadingBooks;
    }

    public static void setCurrentlyReadingBooks(ArrayList<Book> currentlyReadingBooks) {
        Utils2.currentlyReadingBooks = currentlyReadingBooks;
    }

    public static ArrayList<Book> getFavoriteBooks() {
        return favoriteBooks;
    }

    public static void setFavoriteBooks(ArrayList<Book> favoriteBooks) {
        Utils2.favoriteBooks = favoriteBooks;
    }

    public Book getBookById(int id){
        for (Book b: allBooks){
            if(b.getId() == id){
                return b;
            }
        }

        return  null;
    }

    public boolean addToAlreadyRead(Book book){
        return alreadyReadBooks.add(book);
    }
    public boolean addToWantRead(Book book){
        return wantToReadBooks.add(book);
    }

    public boolean delFromAlreadyRead(Book book){
        return alreadyReadBooks.remove(book);
    }
    public boolean delFromWantRead(Book book){
        return wantToReadBooks.remove(book);
    }
}
